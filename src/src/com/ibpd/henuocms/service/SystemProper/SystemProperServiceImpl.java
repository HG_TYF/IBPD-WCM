package com.ibpd.henuocms.service.SystemProper;

import java.util.List;

import org.springframework.stereotype.Service;

import com.ibpd.dao.impl.BaseServiceImpl;
import com.ibpd.henuocms.common.IbpdLogger;
import com.ibpd.henuocms.entity.SystemProperEntity;
@Service("systemProperService")
public class SystemProperServiceImpl extends BaseServiceImpl<SystemProperEntity> implements ISystemProperService {
	private IbpdLogger log=IbpdLogger.getLogger(SystemProperServiceImpl.class);
	public SystemProperServiceImpl(){
		super();
		this.tableName="SystemProperEntity";
		this.currentClass=SystemProperEntity.class;
		this.initOK();
	}

	@Override
	public List<SystemProperEntity> getList() {
		List<SystemProperEntity> list= super.getList();
		if(list==null || list.size()==0){
			init();
			list= super.getList();
		}
		return list;
	}
	public static void main(String[] a){
		for(String key:SystemProperEntity.keys){
			String[] k=key.split("\t");
			System.out.println(k.length);
		}
	}
	private void init(){
		for(String key:SystemProperEntity.keys){
			String[] k=key.split("\t");
			if(k.length<4){
				System.out.println(k.length);
			}
			String keyName=k[0];
			String disName=k[1];
			String type=k[2];
			String def=k[3];
			SystemProperEntity sp=new SystemProperEntity(keyName,disName,def,disName,type);
			this.saveEntity(sp);
		}
	}

	public void saveProperByKeyValue(String key, String value) {
		List<SystemProperEntity> pl=this.getList("from "+this.getTableName()+" where keyName='"+key+"'", null);
		if(pl==null || pl.size()==0)
			return;
		if(pl.size()>1){
			log.error("多余一条的系统配置key[${1}]",key);
		}
		SystemProperEntity sp=pl.get(0);
		sp.setValue(value);
		this.saveEntity(sp);
	}

	public SystemProperEntity getEntityByKey(String key) {
		List<SystemProperEntity> pl=this.getList("from "+this.getTableName()+" where keyName='"+key+"'", null);
		if(pl==null || pl.size()==0)
			return null;
		if(pl.size()>1){
			log.error("多余一条的系统配置key[${1}]",key);
		}
		return pl.get(0);
	}
	
}
 