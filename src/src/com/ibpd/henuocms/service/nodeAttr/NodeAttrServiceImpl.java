package com.ibpd.henuocms.service.nodeAttr;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ibpd.dao.impl.BaseServiceImpl;
import com.ibpd.henuocms.entity.NodeAttrEntity;
@Transactional
@Service("nodeAttrService")
public class NodeAttrServiceImpl extends BaseServiceImpl<NodeAttrEntity> implements INodeAttrService {
	public NodeAttrServiceImpl(){
		super();
		this.tableName="NodeAttrEntity";
		this.currentClass=NodeAttrEntity.class;
		this.initOK();
	} 

	public NodeAttrEntity getNodeAttr(Long nodeId) {
		List<NodeAttrEntity> attrList=this.getList("from "+this.getTableName()+" where nodeId="+nodeId, null);
		if(attrList!=null)
			if(attrList.size()>0)
				return attrList.get(0);
		return null;
	}
	
}
