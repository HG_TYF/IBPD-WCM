package com.ibpd.henuocms.service.field;

import com.ibpd.dao.impl.IBaseService;
import com.ibpd.henuocms.entity.FieldEntity;

public interface IFieldService extends IBaseService<FieldEntity> {

}
 