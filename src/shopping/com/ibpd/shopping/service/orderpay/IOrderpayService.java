package com.ibpd.shopping.service.orderpay;

import com.ibpd.dao.impl.IBaseService;
import com.ibpd.shopping.entity.OrderpayEntity;

public interface IOrderpayService extends IBaseService<OrderpayEntity> {
	OrderpayEntity getOrderpayByOrderId(Long orderId);
	void deleteByOrderId(Long orderId);
}
