<%@ page language="java" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort() + path + "/";

	StringBuffer uploadUrl = new StringBuffer("http://");
	uploadUrl.append(request.getHeader("Host"));
	uploadUrl.append(request.getContextPath());
	uploadUrl.append("/manage/crm/doUpload.do");
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		
		<title>转换退补单导入文件</title>
		<link href="<%=basePath%>css/fileupload.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="<%=basePath%>css/button.css" type="text/css" />
		
		<link rel="stylesheet" type="text/css" href="<%=basePath%>js/ext/resources/css/ext-all.css" />
 		<script type="text/javascript" src="<%=basePath%>js/ext/adapter/ext/ext-base.js"></script>
    	<script type="text/javascript" src="<%=basePath%>js/ext/ext-all.js"></script>
  		<script type="text/javascript" src="<%=basePath%>js/ext/ext-lang-zh_CN.js"></script>
		
		<script type="text/javascript" src="<%=basePath%>js/swfupload/swfupload.js"></script>
		<script type="text/javascript" src="<%=basePath%>js/swfupload/swfupload.queue.js"></script>
		<script type="text/javascript" src="<%=basePath%>js/swfupload/handlers.js"></script>

		<script type="text/javascript" src="<%=basePath%>js/swfupload/swfupload.js"></script>
		<script type="text/javascript" src="<%=basePath%>js/swfupload/handlers.js"></script>
		<script type="text/javascript">
			var swfu;
			window.onload = function () {
				swfu = new SWFUpload({
					upload_url: "<%=uploadUrl.toString()%>",
					post_params: {"uCode" : "all","statistiscs" : "false","split":"2"},//"uCode" : getUCode(),"split" : "false"
					use_query_string : true,
					// File Upload Settings
					file_size_limit : "3 MB",	// 1000MB
					file_types : "*.txt",
					file_types_description:"目前仅仅可以导入规定格式的txt文件",
					file_types_description : "文本文件",
					file_upload_limit : "0",
					prevent_swf_caching:true,
					file_queue_error_handler : fileQueueError,
					file_dialog_complete_handler : fileDialogComplete,//选择好文件后提交
					file_queued_handler : fileQueued,
					upload_progress_handler : uploadProgress,
					upload_error_handler : uploadError,
					upload_success_handler : uploadSuccess,
					upload_complete_handler : uploadComplete,
					file_upload_limit:1,
					// Button Settings
					button_image_url : "<%=basePath%>images/SmallSpyGlassWithTransperancy_17x18.png",
					button_placeholder_id : "spanButtonPlaceholder",
					button_width: 198,
					button_height: 18,
					button_text : '<span class="button">选择文件 <span class="buttonSmall">(最大3MB)</span></span>',
					button_text_style : '.button { font-family: Helvetica, Arial, sans-serif; font-size: 12px; } .buttonSmall { font-size: 10pt; }',
					button_text_top_padding: 0,
					button_text_left_padding: 18,
					button_window_mode: SWFUpload.WINDOW_MODE.TRANSPARENT,
					button_cursor: SWFUpload.CURSOR.HAND,
					
					// Flash Settings
					flash_url : "<%=basePath%>js/swfupload/swfupload.swf",
	
					custom_settings : {
						upload_target : "divFileProgressContainer"
					},
					// Debug Settings
					debug: false  //是否显示调试窗口
				});
			};
			function startUploadFile(){
				swfu.startUpload();
			};
			function showExtShow(){
				win.show();
			};

		</script>
	</head>
	<body style="background-color: #C0D1E3; padding: 2px;">
		<div id="content" style="float:left;margin:0;padding:0;width:100%;margin-top:20px;">
			<form>
				<div
					style="float:left;width:200px;height:20px;display: inline; border: solid 1px #7FAAFF; background-color: #C5D9FF; padding: 2px;">
					<span id="spanButtonPlaceholder"></span>
				</div>
				<div style="float:left;width:170px;height:20px;">
							<input id="btnUpload" type="button" value="上  传"
						onclick="startUploadFile();" class="btn3_mouseout" onMouseUp="this.className='btn3_mouseup'"
						onmousedown="this.className='btn3_mousedown'"
						onMouseOver="this.className='btn3_mouseover'"
						onmouseout="this.className='btn3_mouseout'"/>
					<input id="btnCancel" type="button" value="取消所有上传"
						onclick="cancelUpload();" disabled="disabled" class="btn3_mouseout" onMouseUp="this.className='btn3_mouseup'"
						onmousedown="this.className='btn3_mousedown'"
						onMouseOver="this.className='btn3_mouseover'"
						onmouseout="this.className='btn3_mouseout'"/>
				</div>
			</form>
			<div style="float:left;width:400px;" id="divFileProgressContainer"></div>
			<div style="float:left;width:400px;" id="thumbnails">
				<table id="infoTable" border="0" width="400" style="display: inline; border: solid 1px #7FAAFF; background-color: #C5D9FF; padding: 2px;margin-top:8px;">
				</table>
			</div>
		</div>
		<div id="config" style="width:100%;float:left;margin-top:20px;">
			<input type="checkbox" id="statistiscs" onchange="change()"><span>统计每个商品的退补数量</span><br/>
			<input type="radio" id="use1" name="usesplit" onchange="change()"><span>使用逗号分隔符(导出后可直接执行)</span><br/>
			<input type="radio" id="use2" name="usesplit" onchange="change()"><span>使用TAB分隔符(导出后需要手工替换但可直接用excel打开)</span>
			<br/><span>批量退补原因备注:<input type="text" id="remark" name="remark" value="5002"></span>
		</div> 
			<div id="unitSelect" style="width:100%;float:left;margin-top:20px;display:none;">
				<div id="swap">
				<span>选择门店：如果不做选择则默认为全部门店</span>
				<table>
					<tr>
						<td>
							<div id="left" style="width:200px;height:300px;overflow-y:auto;border:solid 1px #464646;background-color:#f0f0f0;">
							<ul id="swap_left">
							
							</ul>
							</div>
						</td>
						<td style="vertical-align:middle;">
							<input type="button" id="toRight" onclick="allRight()" value="&nbsp;全部往右&nbsp;"/>
							<br/><br/><br/>
							<input type="button" id="toLeft" onClick="allLeft()" value="&nbsp;全部往左&nbsp;"/>
						</td>
						<td>
						<div id="right" style="width:200px;height:300px;overflow-y:auto;border:solid 1px #464646;background-color:#f0f0f0;">
							<ul id="swap_right">
							
							</ul>	
						</div>						
						</td>
					</tr>
				</table>
					
					
				</div>
			</div>
	
	</body>
	<script type="text/javascript">
	function change(){ 
		swfu.setPostParams({"uCode" : getUCode(),"statistiscs" : getStatistiscs(),"split":getSplit(),"remark":document.getElementById("remark").value});
	}
	function getStatistiscs(){ 
		return document.getElementById("statistiscs").checked;
	}
	function getSplit(){
		if(document.getElementById("use1").checked){
			return "1";
		}else{
			return "2";
		}
	}
	 function getUCode(){
	 	var len=document.getElementById("swap_right").children.length;
	 	if(len>0){
	 	 var rtn="";
	 	 for(i=0;i<len;i++){
	 	 	rtn+=document.getElementById("swap_right").children[i].childNodes[0].text+",";
	 	 }
	 	 if(rtn.length>1){
	 	 	rtn=rtn.substring(0,rtn.length-1);
	 	 }
	 	 return rtn;
	 	}else{
	 		return "all";
	 	}
	 }
	 function init(){
	 	var uc="0002,0003,0004,0006,0009,0010,0013,0014,0015,0016,0017,0018,0020,0023,0024,0025,0027,0029,0032,0033,0034,0035,0040,0041,0044,0046,0047,0048,0050,0051,0053,0054,0055,0057,0058,0059";
	 	var ucs=uc.split(",");
	 	document.getElementById("swap_left").innerHTML="";
	 	for(i=0;i<ucs.length;i++){
	 		document.getElementById("swap_left").innerHTML+="<li style='cursor:pointer;'><a href='#' target='_self' onclick='toRight(this)'>"+ucs[i]+"</a></li>";
	 	}
		
	 }
	 init();
	 function toRight(s){
	 	s.parentNode.remove();
	 	document.getElementById("swap_right").innerHTML+="<li style='cursor:pointer;'><a href='#' target='_self' onclick='toLeft(this)'>"+s.innerHTML+"</a></li>";
	 	swfu.setPostParams({"uCode" : getUCode(),"statistiscs" : getStatistiscs(),"split":getSplit()});
	 	return false;
	 }
	 function toLeft(s){
	 	document.getElementById("swap_left").innerHTML+="<li style='cursor:pointer;'><a href='#' target='_self' onclick='toRight(this)'>"+s.innerHTML+"</a></li>";
	 	s.parentNode.remove();
	 	swfu.setPostParams({"uCode" : getUCode(),"statistiscs" : getStatistiscs(),"split":getSplit()});
	 	return false;
	 }
	 function allRight(){
	 	document.getElementById("swap_right").innerHTML+=document.getElementById("swap_left").innerHTML.replace(new RegExp("toRight","gm"),"toLeft") ;
	 	document.getElementById("swap_left").innerHTML="";
	 	swfu.setPostParams({"uCode" : getUCode(),"statistiscs" : getStatistiscs(),"split":getSplit()});
	 }
	 function allLeft(){
	 	init();
	 	document.getElementById("swap_right").innerHTML="";
	 	swfu.setPostParams({"uCode" : getUCode(),"statistiscs" : getStatistiscs(),"split":getSplit()});
	 }
	</script>
</html>